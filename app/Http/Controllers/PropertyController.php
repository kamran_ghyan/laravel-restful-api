<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use App\Image;
use Symfony\Component\HttpFoundation\Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class PropertyController extends Controller
{
    public function __construct()
    {
        //$this->middleware('jwt.auth');
    }

    public function index(Request $request){

        // User input for search
        $search_term = $request->input('search');

        // Set limit for user input
        $limit = $request->input('limit')?$request->input('limit'):10;

        // Check if search terms
        if($search_term){
            $properties = Property::orderBy('id', 'desc')->where('body', 'LIKE', '%$search_term%')->paginate($limit);
        } else{
            $properties = Property::orderBy('id', 'desc')->paginate($limit);
        }


        // Append limit
        $properties->appends(array(
            'limit' => $limit
        ));

        // Convert objects into Array
        $propertiesArray = $properties->toArray();

        // Response
        return response()->json([
            'total' => $propertiesArray['total'],
            'per_page' => $propertiesArray['per_page'],
            'current_page' => $propertiesArray['current_page'],
            'last_page' => $propertiesArray['last_page'],
            'next_page_url' => $propertiesArray['next_page_url'],
            'prev_page_url' => $propertiesArray['prev_page_url'],
            'from' => $propertiesArray['from'],
            'to' => $propertiesArray['to'],
            'data' => $propertiesArray['data']
        ], 200);
    }

    public function show($id){

        $property = Property::find($id);
        // Find a particular property
        if(!$property){
            return response()->json([
                'errpr' => [
                    'message' => 'Property does not exists'
                ]
            ], 400);
        }

        // get previous property id
        $previous = Property::where('id', '<', $property->id)->max('id');

        // get next property id
        $next = Property::where('id', '>', $property->id)->min('id');

        return response()->json([
            'previous_property_id'=> $previous,
            'next_property_id'=> $next,
            'data' => $property
        ], 200);

    }

    public function store(Request $request)
    {
        // Validate the data
        $this->validate($request, array(
            'title'         => 'required|unique:properties,title',
            'body'          => 'required'
        ));
        // Validate posted data
        if(! $request->title or ! $request->body){
            return response()->json([
                'error' => [
                    'message' => 'Please Provide Both title and body'
                ]
            ], 422);
        }

        // Create posted property in databse
        $property = new Property;

        $property->title = $request->title;
        $property->body = $request->body;

        $property->save();

        // Response
        return response()->json([
            'message' => 'Property Created Succesfully',
            'data' => $property
        ]);
    }

    public function update(Request $request, $id)
    {
        // Validate posted data
        if(! $request->title or ! $request->body){
            return response()->json([
                'error' => [
                    'message' => 'Please Provide Both title and body'
                ]
            ], 422);
        }

        // Update property
        $property = Joke::find($id);
        $property->title = $request->title;
        $property->body = $request->body;
        $property->save();

        // Response
        return response()->json([
            'message' => 'Property Updated Succesfully'
        ]);
    }

    public function destroy($id)
    {
        Property::destroy($id);
    }
}
