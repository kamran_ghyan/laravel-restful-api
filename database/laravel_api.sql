-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2016 at 07:28 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `list_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2016_12_19_134356_create_images_table', 1),
(8, '2016_12_19_143238_create_properties_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'Elite Baypoint Community Apartment', 'Property 1 description', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(2, '850 N dewitt 6k', 'Property 2 description', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(3, '850 N Dewitt 19A', 'Property 3 description', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(4, '1250 lasalle 904', 'Property 4 description', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(8, 'Lot 6 Woodland Court, ADDISON 60101', 'NEW CONSTRUCTION ~ Designed for today''s lifestyle', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(9, 'Lot 7 Woodland Court, ADDISON 60101', 'NEW CONSTRUCTION ~ Designed for today''s lifestyle', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(10, 'Lot 14 Woodland Court, ADDISON 60101', 'NEW CONSTRUCTION ~ Designed for today''s lifestyle', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(11, 'Lot 4 Woodland Court, ADDISON 60101', 'NEW CONSTRUCTION ~ Designed for today''s lifestyle', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(12, '1757 Rizzi Lane, BARTLETT 60103', 'Total Luxury! This 4 bedroom maintenance free cond...', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(13, '1837 Rizzi Lane, BARTLETT 60103', 'Stunning and impressive best describes this unique...', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(14, '250 George Street A, BENSENVILLE 60106', 'NICELY MAINTAINED 2 BEDROOM 2 BATH CONDO ON CHALET...', NULL, NULL),
(15, '223 MONARCH Drive, STREAMWOOD 60107', 'Immaculate & Bright Townhome with 9 Ft. 1st Floor ...', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(16, '348 Montabello, BLOOMINGDALE 60108', 'Gorgeous townhouse uniquely designed with spectacu...', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(17, '906B COVENTRY Lane 0, STERLING 61081', 'Spacious 2 story home with vinyl siding and centra...', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(18, '530 North Loves Road, CORTLAND 60112', 'First time on the market. Original owner. Currentl...', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(19, '271 Par Five Drive A, DEKALB 60115', 'LARGE END UNIT OPEN LAYOUT RANCH TOWNHOME WITH FUL...', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(20, '1514 Reserve Lane 3, DEKALB 60115', 'Welcome home!  This lovely Sapphire Model boasts t...', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(21, '12 OAK VALLEY Drive, Cary 60013', 'NEW KITCHEN, BATH, CARPETING, PAINT, MOVE RIGHT IN...', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(22, '549 CARY WOODS Circle, CARY 60013', '549 CARY WOODS Circle, CARY 60013....', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(23, '501 Jenkran Street 7, Morrison 61270', '501 Jenkran Street 7, Morrison 61270....', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(24, '502 Sunset Drive RF, PROPHETSTOWN 61277', '502 Sunset Drive RF, PROPHETSTOWN 61277....', '2016-12-18 19:00:00', '2016-12-18 19:00:00'),
(25, '8500 Sunset Drive RF, PROPHETSTOWN 61277', '8500 Sunset Drive RF, PROPHETSTOWN 61277....', '2016-12-18 19:00:00', '2016-12-18 19:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
